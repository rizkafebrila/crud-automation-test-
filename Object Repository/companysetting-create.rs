<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>create company setting</description>
   <name>companysetting-create</name>
   <tag></tag>
   <elementGuidId>23cbc57a-9c4e-4405-86b3-13994a50cdbe</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;user_data\&quot;: {\n        \&quot;user_id\&quot;: \&quot;13100\&quot;,\n        \&quot;company_id\&quot;: 518,\n        \&quot;user_company_id\&quot;: 2012\n  },\n    \&quot;data\&quot;: {\n      \&quot;third_party\&quot;: \&quot;gfms_cimb\&quot;,\n      \&quot;key\&quot;: \&quot;cobainkey\&quot;,\n      \&quot;value\&quot;: \&quot;haha12345\&quot;,\n      \&quot;credential\&quot;:false\n    }\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://integrationapi.qa/v1/accounting/configuration/companySetting/create/r1?</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
